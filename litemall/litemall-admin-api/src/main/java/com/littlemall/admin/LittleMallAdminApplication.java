package com.littlemall.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.littlemall.db", "com.littlemall.core",
        "com.littlemall.admin"})
@MapperScan("com.littlemall.db.dao")
@EnableTransactionManagement
@EnableScheduling
public class LittleMallAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittleMallAdminApplication.class, args);
    }

}